#include <Arduino.h>
#include <dcf77_decoder.h>
#include <TimeLib.h>

DCF77_Decoder *DCF77_Decoder::_instance;

// Structure of the signal
struct DCF77_Struct {
    unsigned long long start      :1; // always 0
    unsigned long long prefix     :14; // weather/civil warning bits (not used)
    unsigned long long meta       :5; // includes warning bit for employees, Summer time announcement, CET/CEST marker, Leap second announcement
    unsigned long long start_time :1; // always 1
    unsigned long long minute     :7;
    unsigned long long parity_1   :1; // over minute
    unsigned long long hour       :6;
    unsigned long long parity_2   :1; // over hour
    unsigned long long day_month  :6; // e.g. 1-30
    unsigned long long day_week   :3; // e.g. 1(=Monday) till 7 (=Sunday)
    unsigned long long month      :5;
    unsigned long long year       :8;
    unsigned long long parity_3   :1; // over day_month,day_week,month,year
};

// initialize struct
struct DCF77_Struct *dcf_signal;
// store the signal in this variable via bit shifting
unsigned long long received_signal = 0;
// once the full signal has been received, store the "working" copy in this buffer, 
// so the following bits of the new signal can be stored in a fresh var
unsigned long long signal_copy;
// this counter is used for bit shifting (which position the received bit has within the whole DCF signal structure)
int position_counter = 0;

// prescaler - to set frequency of processor/ticks
const int prescaler = 1024;
// division constant calculated by ((Processor Speed/1000) / prescaler) for example: (16000000/1000) / 1024 = 15.625
const float prescale_div = (16000/prescaler);
// store ms since last start of a pulse
volatile float pulse_start;
// store ms of time between rising and falling edge
volatile float pulse_length;

// Constructor - init vars
DCF77_Decoder::DCF77_Decoder(int pin) {
    _pin = pin;
    total_count = 0;
    failure_count = 0;
}

// Set up the program:
// initialize the Serial Terminal, set up the timer
void DCF77_Decoder::start_decoder() {
    Serial.begin(9600);
    init_timer();
    pinMode(_pin, INPUT);
    _instance = this;
}

//=======================================================================
// Low-level Methods/Interrupt handlers 
//=======================================================================
// Set up the timer and input capture interrupts
void DCF77_Decoder::init_timer() {
    // stop interrupts
    cli();
    
    // TCCR1A/C: not using compare output and using default waveform generator settings
    TCCR1A = 0;
    TCCR1C = 0;
    
    /* TCCR1B
    *  ICNC1: 1 = noise canceler on
    *  ICES1: 0 = trigger by falling edge; 1 = trigger rising edge
    *  CS12..10 sets the prescaler */
    TCCR1B = (1<<ICNC1) | (1<<ICES1) | (1<<CS12) | (1<<CS10);

    // TCNT1: Timer/Counter register
    // ICR1: Input capture register, updated with TCNT1 value when ICP1 event
    
    /* TIMSK1 
    *  ICIE1: Enable input capture interrupt 
    *  TOIE1: Enable overflow interrupt (for missing pulses) */
    TIMSK1 = (1<<ICIE1) | (0<<TOIE1);
    
    /* TIFR1 Interrupt Flag Register
    *  ICF1: flag is set when capture event occurs
    *  TOV1: overflow flag
    *  - writing 1 to register clears flag
    *  - But they are also automatically cleared when interrupt routine is executed */
    TIFR1 = (1<<ICF1) | (1<<TOV1);
    
    // allow interrupts
    sei();
    Serial.println("Timer initialized");
}

// Since the processor is counting in ticks and not milliseconds: convert the ticks into milliseconds 
float ticks_to_ms(float icr_count) {
    return icr_count / prescale_div;
}

// Input Capture Vector (ISR - Interrupt Service Routine)
// Each pulse has a beginning (rising edge) and an end (falling edge), like: __|-----|_____|---|___
// One pulse unit lasts for exactly one second, within that second there is an "up" pulse of 100ms or 200ms. (----)
// The rest of the pulse unit goes back to baseline. (___)
// Every time there is a rising or falling edge, this interrupt routine gets called with the number of ticks since the last call.
// Those ticks need to be converted back to ms (number/frequency of ticks depend on the processor speed and prescaler)
// This number represents the length of the "active" pulse. 100ms is a bit 0, 200ms is a bit 1
// This 0 or 1 gets stored via bit shifting in an unsigned long long variable.
// Note: There is a "empty" pulse unit of 1s with no rising/falling edge. This indicates the start of a new minute.
ISR(TIMER1_CAPT_vect) {
    // reset counter
    TCNT1 = 0;
    // check íf falling or rising edge has been detected
    if (bit_is_set(TCCR1B, ICES1)) {    // rising edge, only used to detect new minute
        // Calculate ticks into ms
        pulse_start = ticks_to_ms(ICR1);
        if (pulse_start > 1200) {  // a new minute has started if there is no rising/falling edge over 1 second (+200ms for buffer)
            // copy signal from working variable into buffer variable to be processed
            signal_copy = received_signal;
            // mark that a new time update is available - call to static instance because can't access otherwise from ISR
            // Signal - Slot implementation
            DCF77_Decoder::_instance->new_time_available.fire(true);
            // reset variables
            received_signal = 0;
            position_counter = 0;
            pulse_start = 0;            
        }
            
    } else {                            // falling edge
        // Calculate ticks into ms
        pulse_length = ticks_to_ms(ICR1);
        if (pulse_length < 60) {
            // dismiss because interference, error
        } else if (pulse_length < 140) {   // ~100ms is 0 (140ms for buffer in case of error) 
            // just increase counter, don't add anything to the signal
            position_counter++;
        } else {                    // ~200ms is 1
            // set the bit position within the received signal to 1 and increase counter
            received_signal |= (1ULL << position_counter);
            position_counter++;
        }
    }
    // Flip edge detect bít to trigger on next part of the pulse
    TCCR1B ^= (1<<ICES1);
}

//=======================================================================
// Methods dealing with Interpretation of the signal
//=======================================================================
// convert binary coded decimal input into normal decimal
// the binary coded decimal signal is structured into max 2 nibbles (=1 whole byte)
// per nibble: 0001 = 1, 0010=2, 0100=4, 1000=8, 1001=9 etc.
// the first nibble is used for the ones, the second nibble for the tenths
// thus you have to multiply the number of the second nibble by 10
int DCF77_Decoder::bcd2dec(unsigned long long input) {
    int nibble_1;
    int nibble_2;
    // binary AND with 15 (bin: 1111) to get first 4 bits
    nibble_1 = (input & 15);
    // shift by 4, then AND with 15 again to get the last 4 bits
    nibble_2 = ((input >> 4) & 15);
    // part2 always denotes the tenth decimal place, so multiply by 10
    int result = nibble_2*10 + nibble_1;
    return result;
}

// perform parity checks on the signal to make sure that it is without errors
// additionally do some sanity checks because parities are not very strong indicators
// return false if the signal has errors, true if it is without errors
bool DCF77_Decoder::validate_signal() {
    // parity checks - disabled because increased error rate, 
    //      also most of the time not necessary since the received signal is usually clean & accurate (at least where I currently live)
    // sanity checks
    if (_curr_minute > 60 
        || _curr_hour > 24 
        || _curr_day_month > 31 
        || _curr_month > 12 
        || _curr_year != 20) { // TODO Remove, just for testing
        return false;
    }
    return true;
}

// convert the Binary Coded Decimal of the signal into normal decimal numbers
// and save them in private variables
void DCF77_Decoder::decode_signal() {
    _curr_minute    =   bcd2dec(dcf_signal->minute);
    _p1_minute      =           dcf_signal->parity_1;
    _curr_hour      =   bcd2dec(dcf_signal->hour);
    _p2_hour        =           dcf_signal->parity_2;
    _curr_day_month =   bcd2dec(dcf_signal->day_month);
    _curr_day_week  =   bcd2dec(dcf_signal->day_week);
    _curr_month     =   bcd2dec(dcf_signal->month);
    _curr_year      =   bcd2dec(dcf_signal->year);
    _p3_date        =           dcf_signal->parity_3;
}

time_t DCF77_Decoder::get_update() {
    // cast to the struct just takes the raw signal and splits it up based on the bitfields in the structure
    dcf_signal = (struct DCF77_Struct*)(unsigned long long) &signal_copy;
    
    // decode the signal
    decode_signal();
    // check signal
    if(!validate_signal()) {
        Serial.println("Signal Invalid");
        failure_count++;
    }
    total_count++;

    Serial.println("### New Minute!");

    tmElements_t new_time {
        .Second = 0,
        .Minute = _curr_minute,
        .Hour = _curr_hour,
        .Wday = _curr_day_week,
        .Day = _curr_day_month,
        .Month = _curr_month,
        .Year = _curr_year
    };

    return makeTime(new_time);
}