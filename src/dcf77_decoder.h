/* 
    dcf77_decoder.h - Decoder for the DCF77 longwave signal in Mainflingen near Frankfurt
    using an Arduino Uno with a DCF77 receiver from Conrad: https://www.conrad.de/de/dcf-empfaenger-modul-641138-passend-fuer-serie-c-control-641138.html

    DCF77_Decoder Constructor - pass the pin the receiver is attached to
    start_decoder() - start the decoder (duh)
    loop() - continuously probe whether a new signal has been received (to be changed)
    get_date() - returns the date in the format dd.mm.yy
    get_time() - returns the time in the format hh:mm
    get_day() - returns the day, i.e. Monday, Tuesday, etc.
    signal_num - counter for the number of the signal, increases every time a new valid signal has been received
*/

#ifndef DCF77_DECODER_H
#define DCF77_DECODER_H

#include <Arduino.h>
#include <Callback.h>
#include <TimeLib.h>

class DCF77_Decoder {
    public:
        // Methods
        DCF77_Decoder(int);
        void init_timer();
        void start_decoder();
        time_t get_update();
        // Variables
        int total_count;
        int failure_count;
        bool new_time_avail;
        // Glue for accessing class members from ISR
        static DCF77_Decoder *_instance;
        // Signal - Slot implementation
        Signal<bool> new_time_available;
        
    private:
        // Methods
        int bcd2dec(unsigned long long);
        bool validate_signal();
        void decode_signal();
        bool check_parity(int n[]);
        // Variables
        int _pin;
        int _curr_minute;
        int _p1_minute;
        int _curr_hour;
        int _p2_hour;
        int _curr_day_month;
        int _curr_day_week;
        int _curr_month;
        int _curr_year;  
        int _p3_date;      
};

#endif