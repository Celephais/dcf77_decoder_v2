#include <Arduino.h>
#include <LiquidCrystal.h>
#include <TimeLib.h>
#include <Callback.h>
#include <dcf77_decoder.h>

DCF77_Decoder dcf77_decoder(8);
int signal_count = 0;
int prev_millis = 0;

// FOR LCD: initialize the library by associating any needed LCD interface pin
// with the arduino pin number it is connected to
const int rs = 12, en = 11, d4 = 5, d5 = 4, d6 = 3, d7 = 2;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

// slot-method to handle the "new time" signal
void get_time(bool t) 
{ 
  Serial.println("NEW TIME AVAILIBLE IN SIGNAL SLOT!"); 
  time_t new_time = dcf77_decoder.get_update();
  tmElements_t new_time_elements;
  breakTime(new_time,new_time_elements);
  // need to do this because using setTime(new_time) messes up the day of the week and the year - for some reason?
  setTime(new_time_elements.Hour,new_time_elements.Minute,0,new_time_elements.Day,new_time_elements.Month,new_time_elements.Year);
}



void setup() {
    dcf77_decoder.start_decoder();
    // set up the LCD's number of columns and rows and print an init message
    lcd.begin(16, 2);
    lcd.setCursor(0,0);
    lcd.print("Timer init");

    // Signal - Slot implementation
    FunctionSlot<bool> time_slot(get_time);
    dcf77_decoder.new_time_available.attach(time_slot);
}

/* TODO:
*   - display processing symbol while syncing
*/

void loop() {

    // update display every second
    int ms_diff = millis() - prev_millis;
    if (ms_diff >= 1000) {
       
        prev_millis = millis();
        String time_str = String(hour()) + ":" + String(minute()) + ":" + String(second());
        String date_str = String(day()) + "." + String(month()) + "." + String(year());
        // display on LCD
        lcd.clear();
        lcd.setCursor(0,0);
        lcd.print(time_str);
        lcd.print(" || ");
        lcd.print(dcf77_decoder.total_count);
        lcd.print("-");
        lcd.print(dcf77_decoder.failure_count);
        lcd.setCursor(0,1);
        lcd.print(date_str);
        lcd.print(" - ");
        lcd.print(dayShortStr(weekday()));
        // display on Serial
        Serial.print(dcf77_decoder.total_count);
        Serial.print(" ");
        Serial.println(dcf77_decoder.failure_count);
        Serial.print("      Time: ");    
        Serial.print(time_str);
        Serial.print(" ");
        Serial.print(date_str);
        Serial.print(" - ");
        Serial.print(monthShortStr(month()));
        Serial.print(" - ");
        Serial.println(dayShortStr(weekday()));
    }
}